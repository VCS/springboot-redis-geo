package com.example.redisgeo;

import com.alibaba.fastjson.JSON;
import com.example.redisgeo.bean.CityInfo;
import com.example.redisgeo.service.IGeoService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: RedisGeoApplicationTests
 * @Description: 测试用例
 * @Author 刘 仁
 * @DateTime 2020年7月17日 下午3:59:24
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RedisGeoApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class RedisGeoApplicationTests {

    /** fake some cityInfos */
    private List<CityInfo> cityInfos;

    @Autowired
    private IGeoService geoService;

    @BeforeEach
    public void init() {

        cityInfos = new ArrayList<>();

        cityInfos.add(new CityInfo("hefei", 117.17, 31.52));
        cityInfos.add(new CityInfo("anqing", 117.02, 30.31));
        cityInfos.add(new CityInfo("huaibei", 116.47, 33.57));
        cityInfos.add(new CityInfo("suzhou", 116.58, 33.38));
        cityInfos.add(new CityInfo("fuyang", 115.48, 32.54));
        cityInfos.add(new CityInfo("bengbu", 117.21, 32.56));
        cityInfos.add(new CityInfo("huangshan", 118.18, 29.43));
    }

   /**
    * @Title: testSaveCityInfoToRedis
    * @Description: 测试把所有的标准的坐标信息放到redis中
    * @Author 刘 仁
    * @DateTime 2020年7月17日 下午4:46:40
    */
    @Test
    public void testSaveCityInfoToRedis() {

        System.out.println(geoService.saveCityInfoToRedis(cityInfos));
    }

    /**
     * @Title: testGetCityPos
     * @Description: 获取给定城市的坐标
     * @Author 刘 仁
     * @DateTime 2020年7月17日 下午4:47:41
     */
    @Test
    public void testGetCityPos() {

        System.out.println(JSON.toJSONString(geoService.getCityPos(
                Arrays.asList("anqing", "suzhou", "xxx").toArray(new String[3])
        )));
    }

   /**
    * @Title: testGetTwoCityDistance
    * @Description: 获取两个城市之间的距离
    * @Author 刘 仁
    * @DateTime 2020年7月17日 下午4:48:03
    */
    @Test
    public void testGetTwoCityDistance() {

        System.out.println(geoService.getTwoCityDistance("anqing", "suzhou", null).getValue());
        System.out.println(geoService.getTwoCityDistance("anqing", "suzhou", Metrics.KILOMETERS).getValue());
    }

    /**
     * @Title: testGetPointRadius
     * @Description: 根据给定地理位置坐标获取指定范围内的地理位置集合
     * @Author 刘 仁
     * @DateTime 2020年7月17日 下午4:48:28
     */
    @Test
    public void testGetPointRadius() {

        Point center = new Point(cityInfos.get(0).getLongitude(), cityInfos.get(0).getLatitude());
        Distance radius = new Distance(200, Metrics.KILOMETERS);
        Circle within = new Circle(center, radius);

        System.out.println(JSON.toJSONString(geoService.getPointRadius(within, null)));

        // order by 距离 limit 2, 同时返回距离中心点的距离
        RedisGeoCommands.GeoRadiusCommandArgs args =
                RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance().limit(2).sortAscending();
        System.out.println(JSON.toJSONString(geoService.getPointRadius(within, args)));
    }

    /**
     * @Title: testGetMemberRadius
     * @Description: 根据给定地理位置获取指定范围内的地理位置集合
     * @Author 刘 仁
     * @DateTime 2020年7月17日 下午4:49:24
     */
    @Test
    public void testGetMemberRadius() {

        Distance radius = new Distance(200, Metrics.KILOMETERS);

        System.out.println(JSON.toJSONString(geoService.getMemberRadius("suzhou", radius, null)));

        // order by 距离 limit 2, 同时返回距离中心点的距离
        RedisGeoCommands.GeoRadiusCommandArgs args =
                RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance().limit(2).sortAscending();
        System.out.println(JSON.toJSONString(geoService.getMemberRadius("suzhou", radius, args)));
    }

    /**
     * @Title: testGetCityGeoHash
     * @Description: 获取某个地理位置的 geohash 值
     * @Author 刘 仁
     * @DateTime 2020年7月17日 下午4:50:05
     */
    @Test
    public void testGetCityGeoHash() {

        System.out.println(JSON.toJSONString(geoService.getCityGeoHash(
                Arrays.asList("anqing", "suzhou", "xxx").toArray(new String[3])
        )));
    }
}